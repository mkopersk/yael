#this script should be placed in yael dir
#USAGE: ./install_yael.sh <path to anacodna> <path to libgomp>
anaconda_path=$1
libgomp_location=$2 #/lib64/libgomp.so.1
if [ ! -f $libgomp_location  ]; then
	echo "$libgomp_location not found"
	exit 2
fi
$anaconda_path/bin/conda install swig
ln -s $libgomp_location yael/
./configure.sh --swig=$anaconda_path/bin/swig --enable-numpy --python-cflags=-I$anaconda_path/include/python2.7/
make
echo `pwd` > $anaconda_path/lib/python2.7/site-packages/yael.pth
$anaconda_path/bin/python <<< "import sys 
sys.path.append('.')
from yael import ynumpy
print 'Yael test OK'"
